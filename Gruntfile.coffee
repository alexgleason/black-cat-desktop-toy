module.exports = (grunt) ->
  grunt.loadNpmTasks 'grunt-electron-installer'

  grunt.initConfig
    'create-windows-installer':
      ia32:
        appDirectory: 'dist/black-cat-win32-ia32'
        outputDirectory: 'dist/installers/black-cat-win32-ia32'
        authors: 'Alex Gleason'
        exe: 'black-cat.exe'

  grunt.registerTask 'default', ['create-windows-installer']
