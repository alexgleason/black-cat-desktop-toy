Black Cat
=========

Black Cat is a digitally drawn desktop toy based on [alexgleason/jseyes-electron](https://github.com/alexgleason/jseyes-electron) and created using Krita.

![Screenshot](screenshot.png)

License
=======

All files, aside from the exceptions below, are Copyright 2015 Alex Gleason and licensed under GNU GPL version 3. See the attached LICENSE file for the complete license.

Exceptions:

* `app/xeyes.js` is copyright Felix Milea-Ciobanu [[source](https://github.com/felixmc/jQuery-xeyes)]
* `app/roar.wav` is copyright Mike Koenig [[source](http://soundbible.com/1416-Lion.html)]

Contributing
============

* Install command line tools `electron-prebuilt` and `electron-packager`
* You will also need Wine1.7 to be installed on your system if you aren't using Windows
* Run `npm build`
